# First go to top directory and clone submodules
cd ..
git submodule init && git submodule sync && git submodule init && git submodule update &&

# Build SCL and it's dependencies
# Since SCL is configured for building on Ubuntu by default we need to make
# some changes by copying files over.
cp setup/mac-files/lib-CMakeLists.txt 3rdparty/scl-manips-v2.git/3rdparty/chai3d-3.0/lib/CMakeLists.txt &&
cp setup/mac-files/lib_haptics-CMakeLists.txt 3rdparty/scl-manips-v2.git/3rdparty/chai3d-3.0/lib_haptics/CMakeLists.txt &&
cp setup/mac-files/GLee.h 3rdparty/scl-manips-v2.git/3rdparty/chai3d-3.0/chai3d/graphics/GLee.h &&
cp setup/mac-files/scl_lib-CMakeLists.txt 3rdparty/scl-manips-v2.git/applications-linux/scl_lib/CMakeLists.txt &&
cp setup/mac-files/scl_lib-make_everything.sh 3rdparty/scl-manips-v2.git/applications-linux/scl_lib/make_everything.sh &&

cd 3rdparty/scl-manips-v2.git/applications-linux/scl_lib &&
sh make_everything.sh
