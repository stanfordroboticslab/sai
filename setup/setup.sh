# First go to top directory and clone submodules
cd ..
git submodule init && git submodule sync && git submodule init && git submodule update &&

# Build SCL and it's dependencies
cd 3rdparty/scl-manips-v2.git/applications-linux/scl_lib &&
sh make_release.sh &&
sh make_debug.sh

