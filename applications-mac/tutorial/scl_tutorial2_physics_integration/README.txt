This is an example app. You need to compile (make) it and then run it.

1. Compile with the CMake
$ sh make.sh

2. Run
$ ./scl_tutorial2_physics_integration

(or run with output saved to a log file)
$ ./scl_tutorial2_physics_integration > log.txt

